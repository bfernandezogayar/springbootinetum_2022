package es.inetum.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera(String s, int i) {
		super(s, i);
	}
	
	public Tijera() {
		super(null, 0);
	}

	@Override
	public boolean isMe(int i) {
		return false;
	}

	@Override
	public int comparar(int PiedraPapelTijeraFactory) {
		return 0;
	}
	
}
