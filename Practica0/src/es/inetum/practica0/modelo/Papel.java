package es.inetum.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {

	
	public Papel(String s, int i) {
		super(s, i);
	}
	
	public Papel() {
		super(null, 0);
	}

	@Override
	public boolean isMe(int i) {
		return false;
	}

	@Override
	public int comparar(int PiedraPapelTijeraFactory) {
		return 0;
	}

}
