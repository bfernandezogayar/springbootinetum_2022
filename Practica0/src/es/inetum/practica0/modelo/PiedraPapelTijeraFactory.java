package es.inetum.practica0.modelo;

import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	public static int PIEDRA = 1;
	
	public static int PIEPAPELDRA = 1;
	
	public static int TIJERA = 1;
	
	protected String descripcionResultado;
	
	private static List<PiedraPapelTijeraFactory> elementos;
	
	protected String nombre;
	
	protected int numero;
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PiedraPapelTijeraFactory(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
	}
	
	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public abstract boolean isMe(int i);
	
	public abstract int comparar(int PiedraPapelTijeraFactory);
	
	public static PiedraPapelTijeraFactory getInstance(int i) {
		return null;
	}
	
}
