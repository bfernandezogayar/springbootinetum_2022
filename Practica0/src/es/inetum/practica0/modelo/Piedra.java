package es.inetum.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra(String s, int i) {
		super(s, i);
	}
	
	public Piedra() {
		super(null, 0);
	}

	@Override
	public boolean isMe(int i) {
		return false;
	}

	@Override
	public int comparar(int PiedraPapelTijeraFactory) {
		return 0;
	}
	
}
